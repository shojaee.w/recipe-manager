package com.reza.recipemanager.service;


import com.reza.recipemanager.model.Recipe;
import com.reza.recipemanager.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

     public List<Recipe> getRecipes(Boolean vegetarian, Integer servings, String ingredient, String withoutIngredient, String instructions) {

        return recipeRepository.queryRecipes(vegetarian,servings,ingredient,withoutIngredient,instructions);
    }

    public Optional<Recipe> getRecipeById(Long id) {
        return recipeRepository.findById(id);
    }

    public Recipe addRecipe(Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    public Recipe updateRecipe(Long id, Recipe updatedRecipe) {
        Optional<Recipe> optionalRecipe = recipeRepository.findById(id);
        if (optionalRecipe.isPresent()) {
            updatedRecipe.setId(id);
            return recipeRepository.save(updatedRecipe);
        } else {
            return null;
        }
    }

    public boolean deleteRecipe(Long id) {
        Optional<Recipe> optionalRecipe = recipeRepository.findById(id);
        if (optionalRecipe.isPresent()) {
            recipeRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }


}
