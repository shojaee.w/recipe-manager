package com.reza.recipemanager.repository;

import com.reza.recipemanager.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  RecipeRepository extends JpaRepository<Recipe, Long> {

    @Query("SELECT DISTINCT r FROM Recipe r " +
            "LEFT JOIN r.ingredients i " +
            "WHERE (:vegetarian IS NULL OR r.vegetarian = :vegetarian) " +
            "AND (:servings IS NULL OR r.servings = :servings) " +
            "AND (:ingredient IS NULL OR :ingredient IN (i)) " +
            "AND (:withoutIngredient IS NULL OR NOT EXISTS (SELECT 1 FROM r.ingredients i2 WHERE :withoutIngredient = i2)) " +
            "AND (:instructions IS NULL OR r.instructions LIKE %:instructions%)")
    List<Recipe> queryRecipes(
            @Param("vegetarian") Boolean vegetarian,
            @Param("servings") Integer servings,
            @Param("ingredient") String ingredient,
            @Param("withoutIngredient") String withoutIngredient,
            @Param("instructions") String instructions);


}
