package com.reza.recipemanager.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@RequiredArgsConstructor
public class FullTextIndexRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public void createFullTextIndex() {
        String sql = "CREATE FULLTEXT INDEX IF NOT EXISTS idx_instructions ON recipes(instructions)";
        entityManager.createNativeQuery(sql).executeUpdate();
    }
}

