package com.reza.recipemanager.service;

import com.reza.recipemanager.model.Recipe;
import com.reza.recipemanager.repository.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecipeServiceTest {

    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private RecipeService recipeService;

    private Recipe sampleRecipe;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        sampleRecipe = new Recipe(1L, "Sample Recipe", "Instructions", true, 4, Collections.singleton("Ingredient"));
    }

    @Test
    void testGetAllRecipes() {
        List<Recipe> recipes = Collections.singletonList(sampleRecipe);
        when(recipeRepository.queryRecipes(any(),any(),any(),any(),any())).thenReturn(recipes);

        List<Recipe> result = recipeService.getRecipes(null,null,null,null,null);

        assertEquals(recipes.size(), result.size());
        assertEquals(sampleRecipe, result.get(0));
    }

    @Test
    void testGetRecipeById() {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.of(sampleRecipe));

        Optional<Recipe> result = recipeService.getRecipeById(1L);

        assertTrue(result.isPresent());
        assertEquals(sampleRecipe, result.get());
    }

    @Test
    void testGetRecipeById_NotFound() {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.empty());

        Optional<Recipe> result = recipeService.getRecipeById(1L);

        assertFalse(result.isPresent());
    }

    @Test
    void testAddRecipe() {
        when(recipeRepository.save(any(Recipe.class))).thenReturn(sampleRecipe);

        Recipe result = recipeService.addRecipe(sampleRecipe);

        assertEquals(sampleRecipe, result);
    }

    @Test
    void testUpdateRecipe() {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.of(sampleRecipe));
        when(recipeRepository.save(any(Recipe.class))).thenReturn(sampleRecipe);

        Recipe result = recipeService.updateRecipe(1L, sampleRecipe);

        assertEquals(sampleRecipe, result);
    }

    @Test
    void testUpdateRecipe_NotFound() {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.empty());

        Recipe result = recipeService.updateRecipe(1L, sampleRecipe);

        assertNull(result);
    }


    @Test
    void testDeleteRecipe_NotFound() {
        when(recipeRepository.existsById(anyLong())).thenReturn(false);

        boolean result = recipeService.deleteRecipe(1L);

        assertFalse(result);
        verify(recipeRepository, never()).deleteById(anyLong());
    }

}
