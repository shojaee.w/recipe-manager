package com.reza.recipemanager.integration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.rmi.server.UID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class RecipeIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAddRecipe() throws Exception {
        var uid=new UID().toString();
        String newRecipeJson = "{ \"name\": \"New "+uid +" Recipe\", \"instructions\": \"New Instructions\", \"vegetarian\": true, \"servings\": 4, \"ingredients\": [\"Ingredient 1\", \"Ingredient 2\"] }";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/recipes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newRecipeJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("New "+uid+" Recipe"));
    }
    @Test
   public void testGetAllRecipes() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testUpdateRecipe() throws Exception {
        testAddRecipe();
        String updatedRecipeJson = "{ \"id\": 1, \"name\": \"Updated Recipe\", \"instructions\": \"Updated Instructions\", \"vegetarian\": false, \"servings\": 2, \"ingredients\": [\"Ingredient 1\", \"Ingredient 3\"] }";
        mockMvc.perform(MockMvcRequestBuilders.put("/api/recipes/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedRecipeJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Updated Recipe"));
    }

    @Test
    public void testDeleteRecipe() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/recipes/{id}", 1))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testFilterRecipesByVegetarian() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes?vegetarian=true"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].vegetarian").value(true));
    }

    @Test
    public void testFilterRecipesByServings() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes?servings=4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].servings").value(4));
    }

    @Test
    public void testSearchRecipesByText() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes?instructions=New"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].instructions").value("New Instructions"));
    }

    @Test
    public void testFilterRecipesByIngredients() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes?ingredients=Ingredient 1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].ingredients[0]").value("Ingredient 1"));
    }

    @Test
    public void testNotFoundGetRecipeById() throws Exception {
        testAddRecipe();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recipes/{id}", 10))
                .andExpect(status().isNotFound());

    }


}
