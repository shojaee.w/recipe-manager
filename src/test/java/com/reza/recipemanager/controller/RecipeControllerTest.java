package com.reza.recipemanager.controller;

import com.reza.recipemanager.model.Recipe;
import com.reza.recipemanager.service.RecipeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class RecipeControllerTest {

    @Mock
    private RecipeService recipeService;
    @InjectMocks
    private RecipeController recipeController;
    private MockMvc mockMvc;
   private Recipe sampleRecipe;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(recipeController).build();
        sampleRecipe = new Recipe(1L, "Sample Recipe", "Instructions", true, 4, Collections.singleton("Ingredient"));
    }

    @Test
    public void testGetAllRecipesWithTwoRecipes() throws Exception {
        Recipe recipe1 = new Recipe(1L, "Recipe 1", "Instructions 1", true, 4, Collections.singleton("ingredient1"));
        Recipe recipe2 = new Recipe(2L, "Recipe 2", "Instructions 2", false, 2, Collections.singleton("ingredient2"));

        when(recipeService.getRecipes(any(),any(),any(),any(),any())).thenReturn(Arrays.asList(recipe1, recipe2));

        mockMvc.perform(get("/api/recipes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Recipe 1"))
                .andExpect(jsonPath("$[0].instructions").value("Instructions 1"))
                .andExpect(jsonPath("$[0].vegetarian").value(true))
                .andExpect(jsonPath("$[0].servings").value(4))
                .andExpect(jsonPath("$[0].ingredients[0]").value("ingredient1"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").value("Recipe 2"))
                .andExpect(jsonPath("$[1].instructions").value("Instructions 2"))
                .andExpect(jsonPath("$[1].vegetarian").value(false))
                .andExpect(jsonPath("$[1].servings").value(2))
                .andExpect(jsonPath("$[1].ingredients[0]").value("ingredient2"));

        verify(recipeService, times(1)).getRecipes(any(),any(),any(),any(),any());
    }
    @Test
    void testGetAllRecipes() {
        List<Recipe> recipes = Collections.singletonList(sampleRecipe);
        when(recipeService.getRecipes(any(),any(),any(),any(),any())).thenReturn(recipes);

        ResponseEntity<List<Recipe>> response = recipeController.getAllRecipes(null,null,null,null,null);

        assertEquals(recipes, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Test
    void testGetRecipeById() {
        when(recipeService.getRecipeById(anyLong())).thenReturn(Optional.of(sampleRecipe));

        ResponseEntity<Recipe> response = recipeController.getRecipeById(1L);

        assertEquals(sampleRecipe, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Test
    void testGetRecipeById_NotFound() {
        when(recipeService.getRecipeById(anyLong())).thenReturn(Optional.empty());

        ResponseEntity<Recipe> response = recipeController.getRecipeById(1L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    @Test
    void testAddRecipe() {
        when(recipeService.addRecipe(any(Recipe.class))).thenReturn(sampleRecipe);

        ResponseEntity<Recipe> response = recipeController.addRecipe(sampleRecipe);

        assertEquals(sampleRecipe, response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }
    @Test
    void testUpdateRecipe() {
        when(recipeService.updateRecipe(anyLong(), any(Recipe.class))).thenReturn(sampleRecipe);

        ResponseEntity<Recipe> response = recipeController.updateRecipe(1L, sampleRecipe);

        assertEquals(sampleRecipe, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Test
    void testUpdateRecipe_NotFound() {
        when(recipeService.updateRecipe(anyLong(), any(Recipe.class))).thenReturn(null);

        ResponseEntity<Recipe> response = recipeController.updateRecipe(1L, sampleRecipe);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    @Test
    void testDeleteRecipe() {
        when(recipeService.deleteRecipe(anyLong())).thenReturn(true);

        ResponseEntity<Void> response = recipeController.deleteRecipe(1L);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
    @Test
    void testDeleteRecipe_NotFound() {
        when(recipeService.deleteRecipe(anyLong())).thenReturn(false);

        ResponseEntity<Void> response = recipeController.deleteRecipe(1L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}

