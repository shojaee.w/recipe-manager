# Recipe Manager

## Overview

This is a standalone Java application for managing favorite recipes. It allows users to perform CRUD operations on recipes and filter recipes based on various criteria such as vegetarian status, servings, ingredients, and instructions.

## Installation and Build

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/shojaee.w/recipe-manager
2. **Navigate to Project Directory:**
   ```bash
   cd recipe-manager
3. **Navigate to Project Directory:**
   ```bash
   gradlew clean build
## Running the Application
1. **Run the Application:**
   ```bash
   gradlew bootRun
2. **Accessing the Application:**
   * URL: http://localhost:8080
## Swagger UI and Document
1. **Swagger UI:**
   * URL: http://localhost:8080/swagger-ui/index.html
2. **Swagger Document(JSON)**
   * URL: https://gitlab.com/shojaee.w/recipe-manager/-/blob/main/doc/api-doc.json?ref_type=heads
## H2 Database Console   
1. **Accessing H2 Console:**
   * URL: http://localhost:8080/h2-console
   * JDBC URL: jdbc:h2:mem:recipeDB
   * User Name: sa
   * Password: password
   
## Technical Libraries
* Java: JDK 17
* Spring Boot: 3.2.4
* Database: H2 

## Project Architecture
#### The project follows a layered architecture with separation of concerns
![architect view](./doc/architect-view.JPG)
* Controller Layer: Handles incoming HTTP requests and invokes service methods.
* Service Layer: Implements business logic and interacts with the repository layer.
* Repository Layer: Provides methods for interacting with the database.
* Model Layer: Contains entity classes representing data models.
* Test: Contains unit tests, integration tests, etc.


### Author:
#### Reza
